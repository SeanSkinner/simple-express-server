const express = require('express')
const app = express()
const {PORT = 3000} = process.env
const path = require('path')

app.use(express.static(path.join(__dirname, 'public', 'static')))
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'contact.html'))
})

app.post('/contact', (req, res) => {
    const { data } = req.body
    //created this in a file
    res.json(data)
})

app.listen(PORT, () => console.log('Express has started'))